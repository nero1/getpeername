include config.mak

PREFIX ?=
EXEC_PREFIX ?= $(PREFIX)
BINDIR ?= $(EXEC_PREFIX)/bin

PROGS = getpeername

all: $(PROGS)

install: all
	install -D -m 0755 $(PROGS) $(DESTDIR)$(BINDIR)

clean:
	rm -f $(PROGS)
